//Valmisteluvaihe jossa resetataan visanoikein ja väärinmerkit ja valmistellaan muutama
$("document").ready(function () {
    let points = 0;
    var questions = 0;
    $("#q1_correct").removeClass("fa-check");
    $("#q1_correct").removeClass("fa-times");
    $("#q2_correct").removeClass("fa-check");
    $("#q2_correct").removeClass("fa-times");
    $("#q3_correct").removeClass("fa-check");
    $("#q3_correct").removeClass("fa-times");
    $("#q4_correct").removeClass("fa-check");
    $("#q4_correct").removeClass("fa-times");
    $("#q5_correct").removeClass("fa-check");
    $("#q5_correct").removeClass("fa-times");
    $("#q6_correct").removeClass("fa-check");
    $("#q6_correct").removeClass("fa-times");
    $("#q7_correct").removeClass("fa-check");
    $("#q7_correct").removeClass("fa-times");
    $("#q8_correct").removeClass("fa-check");
    $("#q8_correct").removeClass("fa-times");
    $("#endpage").hide();
    $("#seulpeun").hide();
    $("#peukutus").hide();
    //pisteiden tallennus loppusivulle
    function tallennaPisteet(paikka, pisteet) {
        if (!localStorage.getItem(paikka)) {
            localStorage.setItem(paikka, pisteet);
        } else {
            if (localStorage.getItem(paikka) < pisteet) {
                localStorage.setItem(paikka, pisteet);
            }
        }
    }   
    //visan lopetusfunktio jota callataan aina kysymykseen vastatessa
    function endVisa(){
        
        if (questions >= 8){
            
            $("#questions").hide();
            $("#endpage").show();
            if( points === 8){
                $("#peukutus").show();
                $("#congrats").html("Onnittelut.");
                $("#points").html("Pisteesi olivat " + points + " / 8");
                tallennaPisteet("piste7", points);
            }
            else if(points >= 5){
                $("#peukutus").show();
                $("#congrats").html("Vielä on parantamisen varaa.");
                $("#points").html("Pisteesi olivat " + points + " / 8");
                tallennaPisteet("piste7", points);
            }
            else{
                $("#seulpeun").show();
                $("#congrats").html("Yritäs vielä kerran.");
                $("#points").html("Pisteesi olivat " + points + " / 8");
                tallennaPisteet("piste7", points);
            }
        }
        
    }
    //checkfunktioneita
   $("#q1_check").click(function(){
      if($("#q1-1").is(":checked") && $("#q1-2").is(":checked") && $("#q1-5").is(":checked") && $("#q1-6").is(":checked") && $("#q1-7").is(":checked") && $("#q1-9").is(":checked"))
    {
        $("#q1_correct").removeClass("fa-check");
        $("#q1_correct").removeClass("fa-times");
        $("#q1_correct").addClass("fa-check");
        $("#q1_check").attr("disabled" ,true);
        points++;
        questions++;
        endVisa();
    }
    else{
        $("#q1_correct").removeClass("fa-check");
        $("#q1_correct").removeClass("fa-times");
        $("#q1_correct").addClass("fa-times");
        $("#q1_check").attr("disabled" ,true);
        questions++;
        endVisa();
    }
});
$("#q2_check").click(function(){
     if($("#q2-3").is(":checked")){
        $("#q2_correct").removeClass("fa-check");
        $("#q2_correct").removeClass("fa-times");
        $("#q2_correct").addClass("fa-check");
        $("#q2_check").attr("disabled" ,true);
        points++;
        questions++;
        endVisa();
     }
     else{
        $("#q2_correct").removeClass("fa-check");
        $("#q2_correct").removeClass("fa-times");
        $("#q2_correct").addClass("fa-times");
        $("#q2_check").attr("disabled" ,true);
        questions++;
        endVisa();
     }
});
$("#q3_check").click(function(){
    if($("#q3 option:selected").val() === "1"){
        $("#q3_correct").removeClass("fa-check");
        $("#q3_correct").removeClass("fa-times");
        $("#q3_correct").addClass("fa-check");
        $("#q3_check").attr("disabled" ,true);
        points++;
        questions++;
        endVisa();
     }
     else{
        $("#q3_correct").removeClass("fa-check");
        $("#q3_correct").removeClass("fa-times");
        $("#q3_correct").addClass("fa-times");
        $("#q3_check").attr("disabled" ,true);
        questions++;
        endVisa();
    }
});
$("#q4_check").click(function(){
    if($("#q4 option:selected").val() === "1"){
        $("#q4_correct").removeClass("fa-check");
        $("#q4_correct").removeClass("fa-times");
        $("#q4_correct").addClass("fa-check");
        $("#q4_check").attr("disabled" ,true);
        points++;
        questions++;
        endVisa();
    }
     else{
        $("#q4_correct").removeClass("fa-check");
        $("#q4_correct").removeClass("fa-times");
        $("#q4_correct").addClass("fa-times");
        $("#q4_check").attr("disabled" ,true);
        questions++;
        endVisa();
    }
});
$("#q5_check").click(function(){
    if($("#q5-2").is(":checked")){
        $("#q5_correct").removeClass("fa-check");
        $("#q5_correct").removeClass("fa-times");
        $("#q5_correct").addClass("fa-check");
        $("#q5_check").attr("disabled" ,true);
        points++;
        questions++;
        endVisa();
     }
     else{
        $("#q5_correct").removeClass("fa-check");
        $("#q5_correct").removeClass("fa-times");
        $("#q5_correct").addClass("fa-times");
        $("#q5_check").attr("disabled" ,true);
        questions++;
        endVisa();
    }
});
$("#q6_check").click(function(){
    if($("#q6-1").is(":checked")){
        $("#q6_correct").removeClass("fa-check");
        $("#q6_correct").removeClass("fa-times");
        $("#q6_correct").addClass("fa-check");
        $("#q6_check").attr("disabled" ,true);
        points++;
        questions++;
        endVisa();
    }
     else{
        $("#q6_correct").removeClass("fa-check");
        $("#q6_correct").removeClass("fa-times");
        $("#q6_correct").addClass("fa-times");
        $("#q6_check").attr("disabled" ,true);
        questions++;
        endVisa();
    }
});
$("#q7_check").click(function(){
    if($("#q7 option:selected").val() === "1"){
        $("#q7_correct").removeClass("fa-check");
        $("#q7_correct").removeClass("fa-times");
        $("#q7_correct").addClass("fa-check"); 
        $("#q7_check").attr("disabled" ,true);
        points++;
        questions++;
        endVisa();
    }
     else{
        $("#q7_correct").removeClass("fa-check");
        $("#q7_correct").removeClass("fa-times");
        $("#q7_correct").addClass("fa-times");
        $("#q7_check").attr("disabled" ,true);
        questions++;
        endVisa();
    }
});
$("#q8_check").click(function(){
    if($("#q8 option:selected").val() === "1"){
        $("#q8_correct").removeClass("fa-check");
        $("#q8_correct").removeClass("fa-times");
        $("#q8_correct").addClass("fa-check"); 
        $("#8_check").attr("disabled" ,true);
        points++;
        questions++;
        endVisa();
    }
     else{
        $("#q8_correct").removeClass("fa-check");
        $("#q8_correct").removeClass("fa-times");
        $("#q8_correct").addClass("fa-times");
        $("#q8_check").attr("disabled" ,true);
        questions++;
        endVisa();
    }
    
}); 
});
