        
//Pisteenlaskua ja visan toiminnallisuutta
$(document).ready(function(){
    let pisteet =0;
    let pisteet1 =0;
   $(".answer").click(function(){
      let vastaus = Number($(this).val());
      
      let kysymys = $(this).attr("name");
      
      $("[name=" + kysymys + "]").prop("disabled", true);
      
      
      $("#" + kysymys + "_explanation").show();
      
      if (vastaus === 1){
        $(this).next().addClass("real_answer");
       
        $(this).next().addClass("right");
        pisteet++;
        console.log(pisteet);
      } else {
        
        $(this).next().addClass("real_answer");
        $(this).next().addClass("wrong");
        $("[name=" + kysymys + "][value=1]").next().addClass("right_answer");
      }
   });
   
   
   
   //Visan koodi niin että tulee randomisti esille 
   kysymykset3 = ["#kysymys1", "#kysymys2", "#kysymys3","#kysymys4", "#kysymys5","#kysymys6","#kysymys7","#kysymys8"];
   $(".next3").click(function(){
       
    if (kysymykset3.length > 0){
       let kysymys = getRndInteger(0, kysymykset3.length-1);
       let esille = kysymykset3[kysymys];
       $(esille).show();
       kysymykset3.splice(kysymys,1);
       $(this).parent().hide();
   } else {
       $('#pisteboxi').removeClass('hided');
       $(this).parent().hide();
       $('#loppupisteet').html(pisteet);
       $('#koskettelujumala').hide();
       if (pisteet === 0) {
           $('#lopputulos').html('Kertaa alue uudestaan.');
       } else if (pisteet <= 2) {
           $('#lopputulos').html('Kehno tulos kertaa vielä kerran.');
       } else if (pisteet <= 4) {
           $('#lopputulos').html('Pääsit läpi, JEEE.');
       } else if (pisteet <= 6) {
           $('#lopputulos').html('Hyvä tulos, vielä on kuitenkin parannettavaa.');
       } else if (pisteet === 8) {
           $('#lopputulos').html('Täydet pisteet hyvää työtä!');
       }
   }
   //pisteiden tallennusloppusivulle
   tallennaPisteet('piste3', pisteet);
   });
   function getRndInteger(min, max){
       return Math.floor(Math.random() * (max - min + 1) ) + min;
   }
   
   
   
   //Matikkatehtävän muuttujat
    
       let rnd1 = getRndInteger(1,10);
       let rnd2 = getRndInteger(1,10);
       let rnd3 = getRndInteger(1,10);
       let rnd4 = getRndInteger(1,10);
       let rnd5 = getRndInteger(1,10);
       let rnd6 = getRndInteger(1,10);
       let rnd7 = getRndInteger(1,10);
       let rnd8 = getRndInteger(1,10);
       let rnd9 = getRndInteger(1,10);
       let rnd10 = getRndInteger(1,10);
       let rnd11 = getRndInteger(1,10);
       let rnd12 = getRndInteger(1,10);
       let rnd13 = getRndInteger(1,10);
       let rnd14 = getRndInteger(1,10);
       let rnd15 = getRndInteger(1,10);
       let rnd16 = getRndInteger(1,10);
       $('#k1').html(rnd1);
       $('#k2').html(rnd2);
       $('#k3').html(rnd3);
       $('#k4').html(rnd4);
       $('#k5').html(rnd5);
       $('#k6').html(rnd6);
       $('#k7').html(rnd7);
       $('#k8').html(rnd8);
       $('#k9').html(rnd9);
       $('#k10').html(rnd10);
       $('#k11').html(rnd11);
       $('#k12').html(rnd12);
       $('#k13').html(rnd13);
       $('#k14').html(rnd14);
       $('#k15').html(rnd15);
       $('#k16').html(rnd16);
       
       let kysymys = rnd1 +rnd2;
       let kysymys1 = rnd3+rnd4;
       let kysymys2 = rnd5+rnd6;
       let kysymys3 = rnd7+rnd8;
       let kysymys4 = rnd9 +rnd10;
       let kysymys5 = rnd11+rnd12;
       let kysymys6 = rnd13+rnd14;
       let kysymys7 = rnd15+rnd16;
       
       
       
   
   //matikkavisa koodi, pisteenlasku ja pisteentallennus functio
   $('#makkara').click(function(){
       let vastaus = Number($('#v1').val());
       $('#perkele').html('');
      $("#makkara").attr("disabled", true);
       if(kysymys === vastaus) {
           pisteet1++;
           $('#matikkav').show();
           $('#perkele').html(' Oikein');
       } else {
           $('#matikkav').show();
           $('#perkele').html(' VÄÄRIN, oikea vastaus on ');
           $('#perkelee').html(rnd1 + rnd2);
       }
      
   });
      $('#makkara1').click(function(){
       let vastaus1 = Number($('#v2').val());
       $('#perkele1').html('');
       $("#makkara1").attr("disabled", true);
       if(kysymys1 === vastaus1) {
           pisteet1++;
           $('#matikkav1').show();
           $('#perkele1').html(' Oikein');
           
       } else {
           $('#matikkav1').show();
           $('#perkele1').html(' VÄÄRIN, oikea vastaus on ');
           $('#perkelee1').html(rnd3 + rnd4);
       }
      
   });
   
    $('#makkara2').click(function(){
       let vastaus2 = Number($('#v3').val());
       $('#perkele2').html('');
       $("#makkara2").attr("disabled", true);
       if(kysymys2 === vastaus2) {
           pisteet1++;
           $('#matikkav2').show();
           $('#perkele2').html(' Oikein');
           
       } else {
           $('#matikkav2').show();
           $('#perkele2').html(' VÄÄRIN, oikea vastaus on ');
           $('#perkelee2').html(rnd5 + rnd6);
       }
      
   });
   
   $('#makkara3').click(function(){
       let vastaus3 = Number($('#v4').val());
       $('#perkele3').html('');
       $("#makkara3").attr("disabled", true);
       if(kysymys3 === vastaus3) {
           pisteet1++;
           $('#matikkav3').show();
           $('#perkele3').html(' Oikein');
           
       } else {
           $('#matikkav3').show();
           $('#perkele3').html(' VÄÄRIN, oikea vastaus on ');
           $('#perkelee3').html(rnd7 + rnd8);
       }
       
   });
   
   $('#makkara4').click(function(){
       let vastaus4 = Number($('#v5').val());
       $('#perkele4').html('');
       $("#makkara4").attr("disabled", true);
       if(kysymys4 === vastaus4) {
           pisteet1++;
           $('#matikkav4').show();
           $('#perkele4').html(' Oikein');
           
       } else {
           $('#matikkav4').show();
           $('#perkele4').html(' VÄÄRIN, oikea vastaus on ');
           $('#perkelee4').html(rnd9 + rnd10);
       }
       
   });
   
    $('#makkara5').click(function(){
       let vastaus5 = Number($('#v6').val());
       $('#perkele5').html('');
       $("#makkara5").attr("disabled", true);
       if(kysymys5 === vastaus5) {
           pisteet1++;
           $('#matikkav5').show();
           $('#perkele5').html(' Oikein');
           
       } else {
           $('#matikkav5').show();
           $('#perkele5').html(' VÄÄRIN, oikea vastaus on ');
           $('#perkelee5').html(rnd11 + rnd12);
       }
       
   });
   
       $('#makkara6').click(function(){
       let vastaus6 = Number($('#v7').val());
       $('#perkele6').html('');
       $("#makkara6").attr("disabled", true);
       if(kysymys6 === vastaus6) {
           pisteet1++;
           $('#matikkav6').show();
           $('#perkele6').html(' Oikein');
           
       } else {
           $('#matikkav6').show();
           $('#perkele6').html(' VÄÄRIN, oikea vastaus on ');
           $('#perkelee6').html(rnd13 + rnd14);
       }
       
   });
   
       $('#makkara7').click(function(){
       let vastaus7 = Number($('#v8').val());
       $('#perkele7').html('');
       $("#makkara7").attr("disabled", true);
       if(kysymys7 === vastaus7) {
           pisteet1++;
           $('#matikkav7').show();
           $('#perkele7').html(' Oikein');
           
       } else {
           $('#matikkav7').show();
           $('#perkele7').html(' VÄÄRIN, oikea vastaus on ');
           $('#perkelee7').html(rnd15 + rnd16);
       }
       
   });
   
   $('#loppuun').click(function(){
       $('.odens').hide();
       $('#kapten').html('Sait ' +pisteet1 + '/8 pistettä');
       $('#ok').show();
       tallennaPisteet('piste8',pisteet1);
   });
   
   $('#oldeving').click(function(){
       $('#soija').show();
       $('#oldeving').hide();
   });
   
   function tallennaPisteet(paikka, pisteet) {
        if (!localStorage.getItem(paikka)) {
            localStorage.setItem(paikka, pisteet);
        } else {
            if (localStorage.getItem(paikka) < pisteet) {
                localStorage.setItem(paikka, pisteet);
            }
        }
    }
});


