/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min) ) + min;
}

function tallennaPisteet(paikka, pisteet) {
        if (!localStorage.getItem(paikka)) {
            localStorage.setItem(paikka, pisteet);
        } else {
            if (localStorage.getItem(paikka) < pisteet) {
                localStorage.setItem(paikka, pisteet);
            }
        }
    }
    
        

$(function() {
  let pisteet = 0;
  $('.exp').hide();
  $('.red').hide();
  $(".Aloita").on('click', function() {
      $(".piilotettu").show();
        $(this).hide();
        
  });
    $(".helppo").on('click', function() {
        $(".loppu").show();
      $(".helpot").show();
      $(".nappi").show();
      $(".keskivaikeat").hide();
      $(".vaikeat").hide();
      $(".merkki").hide();
        
    });
        $(".keskivaikea").on('click', function() {
            $(".loppu").show();
      $(".keskivaikeat").show();
      $(".nappi").show();
      $(".helpot").hide();
      $(".vaikeat").hide();
      $(".merkki").hide();
    });
        $(".vaikea").on('click', function() {
            $(".loppu").show();
      $(".vaikeat").show();
      $(".nappi").show();
      $(".helpot").hide();
      $(".keskivaikeat").hide();
      $(".merkki").hide();
    });
  $('input[name="test"]').on('click', function() {

    var mina = $(this);
    let luokka = $(this).attr("class");
    $("."+luokka).prop("disabled", true);
    if (mina.val() === 'ans') {
      mina.parents('.q').nextAll('.exp').first().show();
      mina.parents('.q').nextAll('.red').first().hide();
      $('.red').hide();
      pisteet++;
      $("#kirja1").append("<img src='img/book.png'/>");
      console.log(pisteet);
    } else {
      mina.parents('.q').nextAll('.red').first().show();
      mina.parents('.q').nextAll('.exp').first().hide();
      $("#kirja1").append("<img src='img/talo.png'/>");
    }
    $("#pisteet").html( " " + pisteet+ "/9");
    });
    $(".Lopeta").on('click', function() {
      $(".piilotettu").hide();
      $(".loppusivu").show();
        $(this).hide();
        $("#pisteet").html( " " + pisteet+ "/9");
         tallennaPisteet("piste2",pisteet);
         if (pisteet === 9) {
            $(".kaikki").show();
            $(".paremmin").hide();
           $("#palkinto").append("<img src='img/jani_palkinto.png'/>");
           $(".loppukuva").hide();
        }
        if (pisteet <9 && pisteet >5) {
            $(".melkein").show();
            $(".paremmin").hide();
        }
         if (pisteet === 5) {
            $(".ok").show();
             $(".paremmin").hide();
        }
         if (pisteet<5) {
            $(".paremmin").show();
        }
        
        
    });
    
});

$(function() {
    let eurot = 0;
    let sentit = 0;
    let senttit = 0;
    let eurot2 = 0;
    let n1 = getRndInteger(5,20);
    let n2 = getRndInteger(5,30);
    let n3 = getRndInteger(50,100);
    let n4 = getRndInteger(17,20);
    let n5 = getRndInteger(5,17);
    let n6 = getRndInteger(4,12);
    let n7 = getRndInteger(1,4);
    let n8 = getRndInteger(30,50);
    let n9 = getRndInteger(10,20);
    let n10 = getRndInteger(1,50);
    let n11 = getRndInteger(50,100);
    let n12 = getRndInteger(1,50);
    let n13 = getRndInteger(50,100);
    let n14 = getRndInteger(1,50);
    let n15 = getRndInteger(50,100);
    let n16 = getRndInteger(1,50);
    let n17 = getRndInteger(50,100);

    $("#n1").html(n1 + " ");
    $("#n2").html(n2 + " ");
    $("#n3").html(n3 + " ");
    $("#n4").html(n4 + " ");
    $("#n5").html(n5 + " ");
    $("#n6").html(n6 + " ");
    $("#n7").html(n7 + " ");
    $("#n8").html(n8 + " ");
    $("#n9").html(n9 + " ");
    
    $("#n10").html(n10 + "/");
    $("#n11").html( " " + n11 + " ");
    $("#n12").html(n12 + "/");
    $("#n13").html( " " + n13 + " ");
    $("#n14").html(n14 + "/");
    $("#n15").html( " " + n15 + " "); 
    $("#n16").html(n16 + "/");
    $("#n17").html( " " + n17 + " ");
    let luku = n1 - (n1 * 0.4);
    eurot = Math.floor(luku);
    senttit = luku - eurot;
    senttit.toFixed(2);
    let koko2 = eurot + senttit;
    let v1 = koko2.toFixed(2);
    let luku2 = n2 * 0.30;
    eurot2 = Math.floor(luku2);
    sentit = luku2 - eurot2;
    sentit.toFixed(2);
    let koko = sentit + eurot2;
    let v2 = koko.toFixed(2);
    let v3 = 100 - 50 /n3 * 100;
     v3 = Math.round(v3);
    let v4 = n5 / n4 * 100;
     v4 = Math.round(v4);
    let v5 = n7 / n6 * 100;
     v5 = Math.round(v5);
    let v6 = n9 / n8 * 100;
     v6 = Math.round(v6);
    let v7 = n10 / n11 * 100;
     v7 = Math.round(v7);
    let v8 = n12 / n13 * 100;
     v8 = Math.round(v8);
    let v9 = n14 / n15 * 100;
     v9 = Math.round(v9);
    let v10 = n16 / n17 * 100;
     v10 = Math.round(v10);
    
function markAnswers(){
    let countteri= 0;
    let answers = {
    "q1": [v1, v1 + "€"],
    "q2": [v2, v2 + "€"],
    "q3": [v3 + "%"],
    "q4": [v4 + "%"],
    "q5": [v5 + "%"],
    "q6": [v6 + "%"],
    "q7": [v7 + "%"],
    "q8": [v8 + "%"],
    "q9": [v9 + "%"],
    "q10": [v10 + "%"]

};
    $("input[type='text']").each(function(){
        console.log($.inArray(this.value, answers[this.id]));
        if($.inArray(this.value, answers[this.id]) === -1){
            $(this).parent().append("<img class='merkki' src='img/Red.png'/style='width:35px;height:35px;'>");
        } else {
            $(this).parent().append("<img class='merkki' src='img/check.png'/style='width:35px;height:35px;'>");
            countteri++;
        }
      $(".päätä").on('click', function() {
      $(".tausta").hide();
      $(".piilotettu").hide();
      $(".nappi").hide();
      $(".loppusivu2").show();
        $(this).hide();
        $("#countteri").html( " " + countteri);
        if (countteri === 10) {
            $(".kaikki").show();
            $(".tausta").removeClass("alkukuva");
            $(".paremmin").hide();
            $("#tausta").addClass("se1");
        }
        if (countteri <10 && countteri >5) {
            $(".tausta").removeClass("alkukuva");
            $(".melkein").show();
            $(".paremmin").hide();
            $("#tausta").addClass("se2");
        }
         if (countteri === 5) {
            $(".tausta").removeClass("alkukuva");
            $(".ok").show();
             $(".paremmin").hide();
            $("#tausta").addClass("se3");
        }
         if (countteri < 5) {
            $(".tausta").removeClass("alkukuva");
            $(".paremmin").show();
            $("#tausta").addClass("se4");
        }
        });
        
    });
    tallennaPisteet("piste14",countteri);
}
    
$("form").on("submit", function(e){
    
    $("input", this).attr("disabled", true);
    e.preventDefault();
    markAnswers();
    
    
});
});
