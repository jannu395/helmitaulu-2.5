
$(document).ready(function(){
    let points = 0;
   $(".answer").click(function(){
      let vastaus = Number($(this).val());
      
      let kysymys = $(this).attr("name");
      $("[name=" + kysymys + "]").prop("disabled", true);
      
      
      
      if (vastaus === 1){
        $(this).next().addClass("corect_txt");
        $(this).next().addClass("correct");
        
        
        points++;
        console.log(points);
      } else {
         
        $(this).next().addClass("corect_txt");
        $(this).next().addClass("wrong");
        $("[name=" + kysymys + "][value=1]").next().addClass("correct_answer");
      }
      $("#points1").html(points);
      
   });
    
    kysymykset = ["#quest1", "#quest2", "#quest3", "#quest4", "#quest5", "#quest6", "#quest7", "#quest8"];   
   $(".seuraava").click(function(){
       
    if (kysymykset.length > 0){
       let kysymys = getRndInteger(0, kysymykset.length - 1);
       let magic = kysymykset[kysymys];
       $(magic).show();
       kysymykset.splice(kysymys, 1);
       $(this).parent().hide();
       
   } else {
       $("#pointpageX").removeClass("hide1");
       $(this).parent().hide();
        if(points === 0){
                $("#palautettaX").html("Et edes yrittänyt!");
            }if (points > 0){
                $("#viestiX").html("Oneksi olkoon sait");
                $("#viestiX1").html("/ 8 piestettä");
                $("#palautettaX").html("Parantamisen varaa on!");
            }if(points > 4){
                $("#viestiX").html("Oneksi olkoon sait");
                $("#viestiX1").html("/ 8 piestettä");
                $("#palautettaX").html("Aika hyvin! Vielä voisit kuitenkin parantaa!");
            }if(points === 7){
                $("#viestiX").html("Oneksi olkoon sait");
                $("#viestiX1").html("/ 8 piestettä");
                $("#palautettaX").html("Tiedät jo kaiken!");
            }  
         
   }
       tallennaPisteet("piste13", points);

   });
   
   //lajiutuntemus koodi
   kysymykset3 = ["#kysymys1", "#kysymys2", "#kysymys3", "#kysymys4", "#kysymys5", "#kysymys6", "#kysymys7", "#kysymys8"];
   
   $(".seuraava3").click(function(){
       
    if (kysymykset3.length > 0){
       let kysymys = getRndInteger(0, kysymykset3.length - 1);
       let magic = kysymykset3[kysymys];
       $(magic).show();
       kysymykset3.splice(kysymys, 1);
       $(this).parent().hide();
   } else {
        $("#pointpage").removeClass("hided");
        $(this).parent().hide();
        //palautetta pelaajalle
        if(points === 0){
                $("#palautetta").html("Et edes yrittänyt!");
            }if (points > 0){
                $("#viesti").html("Oneksi olkoon sait");
                $("#viesti1").html("/ 8 piestettä");
                $("#palautetta").html("Parantamisen varaa on!");
            }if(points > 5){
                $("#viesti").html("Oneksi olkoon sait");
                $("#viesti1").html("/ 8 piestettä");
                $("#palautetta").html("Aika hyvin! Vielä voit kuitenkin parantaa lajituntemustasi!");
            }if(points === 8){
                $("#viesti").html("Oneksi olkoon sait");
                $("#viesti1").html(" / 8 piestettä");
                $("#palautetta").html("Tiedät jo kaiken!");
            }  
        }
   
   tallennaPisteet("piste6", points);
   
   });
   function getRndInteger(min, max){
       return Math.floor(Math.random() * (max - min + 1) ) + min;
   }
   //pistetaulukon koodi
   function tallennaPisteet(paikka, pisteet) {
        if (!localStorage.getItem(paikka)) {
            localStorage.setItem(paikka, pisteet);
        } else {
            if (localStorage.getItem(paikka) < pisteet) {
                localStorage.setItem(paikka, pisteet);
            }
        }
    }
    //tyhmä matikka pelin koodi lisäys
     let number1 = getRndInteger(3,10);
       $("#juttu2").html(number1);
       let jeejoomoi = number1 * 4;
    $("#matikka").click(function(){
       let vastaus = Number($("#juttu1").val());
      $("#moikkamoi").html('');
      $("#matikka").attr("disabled", true);
       if(jeejoomoi === vastaus) {
           $("#enkeksi").show();
           $("#moikkamoi").html("Oikein!");
           
           points++;
       } else {
           $("#enkeksi").show();
           $("#moikkamoi").html("Vastauksesi on väärin. Oikea vastaus on ");
           $("#vastaus2").html(jeejoomoi);
       }
      
   });
    
});
