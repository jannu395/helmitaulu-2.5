/* 
 *Author: Marjaana Gummerus
 */

$(document).ready(function () {
    /**
     * Generates a random number in a min - max range
     * 
     * @param {Number} min  minimum value for a random number
     * @param {Number} max  maximum value for a random number
     * @returns {Number}    generated random number
     */
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    /*ainevisan tarkistus*/
    let tulos = 0;
    let lehti = '<span class="fa fa-pagelines text center pr-1" aria-hidden="true" style="font-size: 35px; color: #40ff00;"></span>';
    let yrmy = '<span class="fa fa-frown-o text-center pr-1" aria-hidden="true" style="font-size: 35px; color: red;"></span>';
    let pysti1 = '<span class="fa fa-trophy text center" aria-hidden="true" style="font-size: 300px; color: gold;"></span>';
    let pysti2 = '<span class="fa fa-trophy text center" aria-hidden="true" style="font-size: 300px; color: silver;"></span>';
    let pysti3 = '<span class="fa fa-trophy text center" aria-hidden="true" style="font-size: 300px; color: #eb4614;"></span>';
    let surku = '<span class="fa fa-frown-o text-center" aria-hidden="true" style="font-size: 300px;"></span>';
    $(".vastaus").click(function () {
        let ans = Number($(this).val());

        let kysymys = $(this).attr("name");
        $("[name=" + kysymys + "]").prop("disabled", true);

        if (ans === 1) {
            $(this).next().addClass("answer_text");
            $(".oikein").show();
            $("#kuvake").append(lehti);
            tulos++;
        } else {
            $(this).next().addClass("answer_text");
            $(".vaarin").show();
            $("#kuvake").append(yrmy);
        }
    });

    /* aine visan kysymysten esille tuonti*/
    kysymykset = ["#kys", "#kys1", "#kys2", "#kys3", "#kys4", "#kys5", "#kys6", "#kys7", "#kys8", "#kys9"];
    $(".seuraava").click(function () {
        if (kysymykset.length > 0) {
            let esille = kysymykset[0];
            $(esille).show();
            kysymykset.splice(0, 1);
            $(this).parent().hide();
            $(".oikein").hide();
            $(".vaarin").hide();
            $("#kuvake").removeClass("hidden");
        } else {
            /*vastattujen kysymysten piilotus*/
            $(this).parent().hide();
            $("#kuvake").addClass("hidden");
            $("#loppusivu").show(); 
            $("#points").html(tulos);
            
            /*palkintokuvakkeen ja palautteen tulostus*/
            if(tulos === 0){
                $("#loppukuve").append(surku);
                $("#palaute").html("Et vastannut oikein yhteenkään kysymykseen. Tietämyksessäsi on vielä kehittämisen varaa!");
            } else if(tulos < 6){
                $("#loppukuve").append(pysti3);
                $("#palaute").html("Sait pronssisen pokaalin! <br> Tiesit melko hyvin vastauksia visan kysymyksiin!");
            } else if(tulos < 9){
                $("#loppukuve").append(pysti2);
                $("#palaute").html("Sait hopeisen pokaalin! <br> Tietämyksesi on jo melkein huippuluokkaa!");
            } else {
                $("#loppukuve").append(pysti1);
                $("#palaute").html("Sait kultaisen pokaalin! <br> Olet melkoinen biologian tietäjä!");
            }
        }
        tallennaPisteet("piste4", tulos);
    });
    
    /*takasivun pisteet*/
    // Tarkistaa, onko pelikerta ensimmäinen ja tallentaa pisteet
    // Jos ei ole eka kerta, vertaa aikaisempiin pisteisiin
    function tallennaPisteet(paikka, pisteet) {
        if (!localStorage.getItem(paikka)) {
            localStorage.setItem(paikka, pisteet);
        } else {
            if (localStorage.getItem(paikka) < pisteet) {
                localStorage.setItem(paikka, pisteet);
            }
        }
    }

  /*matikkavisan koodit
   * tehtävien tarkistus*/
    let pisteet = 0;
    let hymy = '<span class="fa fa-smile-o text center pr-1" aria-hidden="true" style="font-size: 35px; color: #40ff00;"></span>';
    $("#tarkistus1").click(function () {
        let eka_luku = Number($("[name=luku]:checked").val());
        let vastaus = Number($("[name=ans]").val());
        let kerroin = Number($("#toka_nro").html());

        let lasku = eka_luku * kerroin;

        if (lasku === vastaus) {
            $(".oikein").show();
            $("#menestys").append(hymy);
            pisteet++;
        } else {
            $(".vaarin").show();
            $("#menestys").append(yrmy);
        }
        
        $("#tarkistus1").attr("disabled", true);
    });
    
        $("#tarkistus2").click(function () {
        let eka_luku = Number($("[name=luku]:checked").val());
        let vastaus = Number($("[name=ans1]").val());
        let kerroin = Number($("#kol_nro").html());

        let lasku = eka_luku * kerroin;

        if (lasku === vastaus) {
            $(".oikein").show();
            $("#menestys").append(hymy);
            pisteet++;
        } else {
            $(".vaarin").show();
            $("#menestys").append(yrmy);
        }
        
        $("#tarkistus2").attr("disabled", true);
    });
    
    $("#tarkistus3").click(function () {
        let eka_luku = Number($("[name=luku]:checked").val());
        let vastaus = Number($("[name=ans2]").val());
        let kerroin = Number($("#nel_nro").html());

        let lasku = eka_luku * kerroin;

        if (lasku === vastaus) {
            $(".oikein").show();
            $("#menestys").append(hymy);
            pisteet++;
        } else {
            $(".vaarin").show();
            $("#menestys").append(yrmy);
        }
        
        $("#tarkistus3").attr("disabled", true);
    });
    
    $("#tarkistus4").click(function () {
        let eka_luku = Number($("[name=luku]:checked").val());
        let vastaus = Number($("[name=ans3]").val());
        let kerroin = Number($("#viis_nro").html());

        let lasku = eka_luku * kerroin;

        if (lasku === vastaus) {
            $(".oikein").show();
            $("#menestys").append(hymy);
            pisteet++;
        } else {
            $(".vaarin").show();
            $("#menestys").append(yrmy);
        }
        
        $("#tarkistus4").attr("disabled", true);
    });
    
    $("#tarkistus5").click(function () {
        let eka_luku = Number($("[name=luku]:checked").val());
        let vastaus = Number($("[name=ans4]").val());
        let kerroin = Number($("#kuus_nro").html());

        let lasku = eka_luku * kerroin;

        if (lasku === vastaus) {
            $(".oikein").show();
            $("#menestys").append(hymy);
            pisteet++;
        } else {
            $(".vaarin").show();
            $("#menestys").append(yrmy);
        }
        
        $("#tarkistus5").attr("disabled", true);
    });
    
    $("#tarkistus6").click(function () {
        let eka_luku = Number($("[name=luku]:checked").val());
        let vastaus = Number($("[name=ans5]").val());
        let kerroin = Number($("#sei_nro").html());

        let lasku = eka_luku * kerroin;

        if (lasku === vastaus) {
            $(".oikein").show();
            $("#menestys").append(hymy);
            pisteet++;
        } else {
            $(".vaarin").show();
            $("#menestys").append(yrmy);
        }
        
        $("#tarkistus6").attr("disabled", true);
    });
    
    $("#tarkistus7").click(function () {
        let eka_luku = Number($("[name=luku]:checked").val());
        let vastaus = Number($("[name=ans6]").val());
        let kerroin = Number($("#kas_nro").html());

        let lasku = eka_luku * kerroin;

        if (lasku === vastaus) {
            $(".oikein").show();
            $("#menestys").append(hymy);
            pisteet++;
        } else {
            $(".vaarin").show();
            $("#menestys").append(yrmy);
        }
        
        $("#tarkistus7").attr("disabled", true);
    });
    
    $("#tarkistus8").click(function () {
        let eka_luku = Number($("[name=luku]:checked").val());
        let vastaus = Number($("[name=ans7]").val());
        let kerroin = Number($("#ys_nro").html());

        let lasku = eka_luku * kerroin;

        if (lasku === vastaus) {
            $(".oikein").show();
            $("#menestys").append(hymy);
            pisteet++;
        } else {
            $(".vaarin").show();
            $("#menestys").append(yrmy);
        }
        
        $("#tarkistus8").attr("disabled", true);
    });
    
    $("#tarkistus9").click(function () {
        let eka_luku = Number($("[name=luku]:checked").val());
        let vastaus = Number($("[name=ans8]").val());
        let kerroin = Number($("#kym_nro").html());

        let lasku = eka_luku * kerroin;

        if (lasku === vastaus) {
            $(".oikein").show();
            $("#menestys").append(hymy);
            pisteet++;
        } else {
            $(".vaarin").show();
            $("#menestys").append(yrmy);
        }
        
        $("#tarkistus9").attr("disabled", true);
    });
    
    $("#tarkistus10").click(function () {
        let eka_luku = Number($("[name=luku]:checked").val());
        let vastaus = Number($("[name=ans9]").val());
        let kerroin = Number($("#yyt_nro").html());

        let lasku = eka_luku * kerroin;

        if (lasku === vastaus) {
            $(".oikein").show();
            $("#menestys").append(hymy);
            pisteet++;
        } else {
            $(".vaarin").show();
            $("#menestys").append(yrmy);
        }
        
        $("#tarkistus10").attr("disabled", true);
    });
    
        /*koodi toisen kertojan valintaan*/
    $("[name=luku]").click(function () {
        if ($(this).prop('checked') === true) {
            $("#eka_nro").html($(this).val());
            $("#eka_nro1").html($(this).val());
            $("#eka_nro2").html($(this).val());
            $("#eka_nro3").html($(this).val());
            $("#eka_nro4").html($(this).val());
            $("#eka_nro5").html($(this).val());
            $("#eka_nro6").html($(this).val());
            $("#eka_nro7").html($(this).val());
            $("#eka_nro8").html($(this).val());
            $("#eka_nro9").html($(this).val());
        }
        $(".oikein").hide();
        $(".vaarin").hide();
    });

    /*helppojen tehtävien koodi
     * tehtävien esille tuonti*/
    let mysykykset = 0;
    helppo = ["#teht1", "#teht2", "#teht3", "#teht4", "#teht5", "#teht6", "#teht7", "#teht8", "#teht9", "#teht10"];
    $(".helppo").click(function () {
        $(".oikein").hide();
        $(".vaarin").hide();
        
        $("#kys_print").show();
        
        $("#laskin").addClass("hidden");
        
        if (helppo.length > 0) {
            let esiin = helppo[0];
            $(esiin).show();
            helppo.splice(0, 1);
            $(".helppo").show();
            $(this).parent().hide();

            let numero = getRndInteger(2, 5);
            $("#toka_nro").html(numero);
            $("#kol_nro").html(numero);
            $("#nel_nro").html(numero);
            $("#viis_nro").html(numero);
            $("#kuus_nro").html(numero);
            $("#sei_nro").html(numero);
            $("#kas_nro").html(numero);
            $("#ys_nro").html(numero);
            $("#kym_nro").html(numero);
            $("#yyt_nro").html(numero);

            $("#kys_print1").show();

            $("#valikko").show();
            
            $(".pisteet").removeClass("hidden");
            
            $("#mini_laskin").removeClass("hidden");
            
            mysykykset++;
            $("#kyssarit1").html(mysykykset);
        } else {
            /*vastattujen kysymysten piilotus*/
            $(this).parent().hide();
            $("#valikko").hide();
            $("#loppusivu").show();
            $(".pisteet1").html(pisteet);
            
            $("#kys_print1").hide();
            
            $(".pisteet").addClass("hidden");
            
            $("#mini_laskin").addClass("hidden");
            
            /*palkintokuvakkeen ja palautteen tulostus*/
            if(pisteet === 0){
                $("#loppukuva").append(surku);
                $("#palaute1").html("Et vastannut oikein yhteenkään kysymykseen. Harjoittele vielä lisää kertolaskuja!");
            } else if(pisteet < 3){
                $("#loppukuva").append(pysti3);
                $("#palaute1").html("Sait pronssisen pokaalin!");
            } else if(pisteet < 5){
                $("#loppukuva").append(pysti2);
                $("#palaute1").html("Sait hopeisen pokaalin!");
            } else {
                $("#loppukuva").append(pysti1);
                $("#palaute1").html("Sait kultaisen pokaalin!");
            }
        }
        tallennaPisteet("piste10", pisteet);
    });

    /*vaikeiden tehtävien koodi
     * tehtävien esille tuonti*/
    vaikea = ["#teht1", "#teht2", "#teht3", "#teht4", "#teht5", "#teht6", "#teht7", "#teht8", "#teht9", "#teht10"];
    $(".vaikea").click(function () {
        $(".oikein").hide();
        $(".vaarin").hide();

        $("#laskin").addClass("hidden");

        if (vaikea.length > 0) {
            let esiin = vaikea[0];
            $(esiin).show();
            vaikea.splice(0, 1);
            $(".vaikea").show();
            $(this).parent().hide();
            
            $("#kys_print3").show();

            let numero = getRndInteger(6, 10);
            $("#toka_nro").html(numero);
            $("#kol_nro").html(numero);
            $("#nel_nro").html(numero);
            $("#viis_nro").html(numero);
            $("#kuus_nro").html(numero);
            $("#sei_nro").html(numero);
            $("#kas_nro").html(numero);
            $("#ys_nro").html(numero);
            $("#kym_nro").html(numero);
            $("#yyt_nro").html(numero);

            $("#valikko").show();
            
            $(".pisteet").removeClass("hidden");
            
            $("#mini_laskin").removeClass("hidden");
            
            mysykykset++;
        $("#kyssarit3").html(mysykykset);
        } else {
            /*vastattujen kysymysten piilotus*/
            $(this).parent().hide();
            $("#valikko").hide();
            $("#loppusivu").show();
            $(".pisteet3").html(pisteet);
            
            $("#kys_print3").hide();
            
            $(".pisteet").addClass("hidden");
            
            $("#mini_laskin").addClass("hidden");
            
            /*palkintokuvakkeen ja palautteen tulostus*/
            if(pisteet === 0){
                $("#loppukuva").append(surku);
                $("#palaute3").html("Et vastannut oikein yhteenkään kysymykseen. Harjoittele vielä lisää kertolaskuja!");
            } else if(pisteet < 6){
                $("#loppukuva").append(pysti3);
                $("#palaute3").html("Sait pronssisen pokaalin!");
            } else if(pisteet < 9){
                $("#loppukuva").append(pysti2);
                $("#palaute3").html("Sait hopeisen pokaalin!");
            } else {
                $("#loppukuva").append(pysti1);
                $("#palaute3").html("Sait kultaisen pokaalin!");
            }
        }
        tallennaPisteet("piste10", pisteet);
    });
});

