/* 
 * Name: Oskar Kallunki
 */

$(document).ready(function () {

    // Maantietoscriptit
    // Globaalit muuttujat tehtävänumerolle, pisteille ja vastausten määrälle
    let kysymys_nro = 0;
    let pisteet = 0;
    let vastatut = 0;

    // Satunnaislukugeneraattori
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    // Popoverin koodi
    $(".apu").popover({
        trigger: "hover"
    });

    // Kysymyspaketti arrayssa, muotoa [kysymyksen indeksi[sisältö]]
    let kyssarit = [
        ["Kuinka monta rajanaapuria on Ranskalla?", "2", "4", "8", "16", "Useat pikkuvaltiot, kuten Andorra, lisäävät naapurien määrää.", "8"],
        ["Mikä seuraavista ei ole Euroopan valtio?", "Moldova", "Turkki", "Israel", "Montenegro", "Israel sijaitsee Lähi-idässä.", "Israel"],
        ["Mikä on Unkarin pääkaupunki?", "Budapest", "Varsova", "Bukarest", "Helsinki", "Budapest on tunnettu ja suosittu matkailukohde.", "Budapest"],
        ["Minkä maan lippu kuvassa näkyy?", "Itävalta", "Puola", "Ruotsi", "Sveitsi", "Sveitsin lipun värit ja muoto ovat peräisin Rooman valtakunnasta.", "Sveitsi"],
        ["Paljonko valtioita Euroopassa on?", "alle 40", "noin 50", "noin 60", "tasan 100", "Laskemalla tai tietämällähän se selviää!", "noin 50"],
        ["Missä päin Eurooppaa Belgia sijaitsee?", "Itä-Euroopassa", "Länsi-Euroopassa", "Etelä-Euroopassa", "Keski-Euroopassa", "Belgia on Euroopan länsirannikolla.", "Länsi-Euroopassa"],
        ["Mikä seuraavista lukeutuu Baltian maihin?", "Valko-Venäjä", "Albania", "Liettua", "Norja", "Viro, Latvia ja Liettua muodostavat Baltian alueen.", "Liettua"],
        ["Minkä niminen on Alppien korkein huippu?", "Mont Blanc", "Elbrus", "Aconcagua", "Korvatunturi", 'Huippu tunnetaan myös nimellä ”La Dame Blanche”.', "Mont Blanc"],
        ["Kuinka pitkä Rein-joki on?", "5000 km", "1230 km", "640 km", "111 km", "Rein-joki on Euroopan pisin joki. Se virtaa useiden maiden läpi aina Alankomaista Italiaan.", "1230 km"],
        ["Mikä on Euroopan paras valtio?", "Venäjä", "Ruotsi", "Vatikaani", "Suomi", "Tietysti se on kotimaamme!", "Suomi"]
    ];

    // Visan kuvat ja kuvatekstit, muotoa [kuvan indeksi[sisältö]]
    let kuvat = [
        ['img/france.jpg', 'Hiljainen tienvarsi Ranskassa'],
        ['img/sheep.jpg', 'Lampaita Montenegrossa - ovatko eurooppalaisia?'],
        ['img/hungary.jpg', 'Unkarin upea pääkaupunki'],
        ['img/switzerland.jpg', 'Hieno lippu - mutta mikä maa?'],
        ['img/map.jpg', 'Melkoinen karttapallo'],
        ['img/belgium.jpg', 'Yö belgialaisessa  Namurin kaupungissa'],
        ['img/narva.jpg', 'Viro on yksi Baltian maista - kuvassa Narvan museo.'],
        ['img/mont_blanc.jpg', 'Alppien hulppeat huiput'],
        ['img/rhine.jpg', 'Näkymä Rein-joesta'],
        ['img/vatican.jpg', 'Vilkas Vatikaani']
    ];

    // Lista, jota myöhemmin käytetään kysymysten palauttamiseen kyssarit-listaan.
    let varapakkaus = [];

    // Lista, jota myöhemmin käytetään kuvien palauttamiseen kuvat-listaan.
    let varapakkaus_kuvat = [];

    // Luo ja tuo uuden kysymyksen näkyviin
    function newQuestion() {
        let kys = getRndInteger(0, kyssarit.length - 1);
        kysymys_nro += 1;

        $("#kysymys").html(kyssarit[kys][0]);
        $("#selitys").html(kyssarit[kys][5]).hide();
        $("#seuraava").hide();
        $("#kysymys_nro").html(kysymys_nro);
        $("[name=jaa]").prop("disabled", false);
        $("#ohita").prop("disabled", false);
        $("[name=jaa]").removeClass("oikein");
        $("[name=jaa]").removeClass("vaarin");
        $(".ov").html("");
        $("#kuva").attr("src", kuvat[kys][0]);
        $("#kuvateksti").html(kuvat[kys][1]);

        // Lisää vaihtoehtotekstit nappeihin ja selvittää mikä niistä on oikea ratkaisu
        for (let i = 1; i < 5; i++) {
            $("#jaa" + i).html(kyssarit[kys][i]);

            if (kyssarit[kys][i] === kyssarit[kys][6]) {
                $("#jaa" + i + "value").val(1);
            } else {
                $("#jaa" + i + "value").val(2);
            }
        }

        // Poistaa listalta kysymyksen, lisää sen varapakettiin talteen
        varapakkaus.push(kyssarit[kys]);
        kyssarit.splice(kys, 1);

        // Sama kuville
        varapakkaus_kuvat.push(kuvat[kys]);
        kuvat.splice(kys, 1);
    }

    // Kysymysosion aloitusnappula
    $("#aloita").click(function () {
        $("#maantieto").hide();
        $(".visa").show();
        newQuestion();
    });

    // Kun painetaan jotakin vastausvaihtoehtoa
    $("[name=jaa]").click(function () {
        let vastaus = Number($(this).val());
        $("[name=jaa]").prop("disabled", true);
        $("#ohita").prop("disabled", true);
        $("#selitys").show();
        $("#seuraava").show();
        vastatut += 1;

        // Tarkistaa onko vastaus oikein/väärin ja seuraukset
        if (vastaus === 1) {
            $(this).addClass("oikein");
            $(this).next().html('<span>Oikein! </span><span class="fa fa-check fa-lg" aria-hidden="true"></span>');
            pisteet += 1;
        } else {
            $(this).addClass("vaarin");
            $(this).next().html('<span>Väärin! </span><span class="fa fa-times fa-lg" aria-hidden="true"></span>');
            $("[name=jaa][value=1]").addClass("oikein");
        }
    });

    // Päättää visailun ja palkinto tulee esiin
    function endQuiz() {
        $(".visa").hide();
        $(".loppu").show();
        $("#vastatut").html(vastatut);
        $("#pisteet").html(pisteet);

        if (pisteet === 10 || pisteet === 9) {
            // Kultaa
            $("#ilme").html('<span class="fa fa-smile-o fa-5x mt-3" aria-hidden="true"></span><span class="fa fa-thumbs-o-up fa-5x mt-3 ml-3 swing" aria-hidden="true"></span>');
            $("#lause").html("Tiedät tosi paljon. Loistavaa!");
            $("#palkinto").html('<img src="img/gold.jpg" alt="Kultaa" class="img-fluid"><p class="pl-1">Kultaa palkinnoksi</p>');
        } else if (pisteet < 9 && pisteet > 3) {
            // Hopeaa
            $("#ilme").html('<span class="fa fa-smile-o fa-5x mt-3" aria-hidden="true"></span><span class="fa fa-thumbs-o-up fa-5x mt-3 ml-3 swing" aria-hidden="true"></span>');
            $("#lause").html("Tiedät paljon. Hienoa!");
            $("#palkinto").html('<img src="img/silver.jpg" alt="Hopeaa" class="img-fluid"><p class="pl-1">Hopeaa palkinnoksi</p>');
        } else if (pisteet < 4 && pisteet > 0) {
            // Pronssia
            $("#ilme").html('<span class="fa fa-smile-o fa-5x mt-3" aria-hidden="true"></span><span class="fa fa-thumbs-o-up fa-5x mt-3 ml-3 swing" aria-hidden="true"></span>');
            $("#lause").html("Tiedät sentään jotakin. Hyvä!");
            $("#palkinto").html('<img src="img/bronze.jpg" alt="Pronssia" class="img-fluid"><p class="pl-1">Pronssia palkinnoksi</p>');
        } else if (pisteet === 0) {
            // NASSE-SETÄ
            $("#ilme").html('<span class="fa fa-frown-o fa-5x mt-3" aria-hidden="true"></span><span class="fa fa-thumbs-o-down fa-5x mt-3 ml-3 swing" aria-hidden="true"></span>');
            $("#lause").html("NASSE-SETÄ ON HYVIN VIHAINEN!!!");
            $("#palkinto").html('<iframe controls autoplay allow="autoplay" src="https://www.youtube.com/embed/h5IjOfyy1bY?&autoplay=1" alt="Nasse-setä"></iframe>');
        }

        tallennaPisteet("piste5", pisteet);

    }

    // Seuraava kysymys - sekä Seuraava että Ohita -napille
    $("#seuraava, #ohita").click(function () {
        if (kyssarit.length > 0) {
            newQuestion();
        } else {
            endQuiz();
        }
    });

    // Pelaa uudelleen - Laittaa kysymykset ja kuvat takaisin alkuperäisiin listoihin ja tyhjentää varapakkaukset, mitkä täyttyvät taas kun pelataan uudestaan
    $("#uudelleen").click(function () {
        $(".loppu").hide();
        $(".visa").show();
        kysymys_nro = 0;
        vastatut = 0;
        pisteet = 0;
        kyssarit = varapakkaus;
        kuvat = varapakkaus_kuvat;
        varapakkaus = [];
        varapakkaus_kuvat = [];
        newQuestion();
        $("#palkinto").html("");
    });

    // Lukujonoscriptit
    // Globaalit muuttujat
    let tehtava_nro = 0;
    let vaikeus = 0;
    let pallot = 0;
    let nro = 0;
    let tehdyt = [];
    let pisteet2 = 0;
    let vastatut2 = 0;

    // Vatun poskaa
    $("#perseily").click(function () {
        localStorage.setItem("perseily", "on");
        window.location.href = 'kallunki_matikka.html';
    });

    // Lisää pastaa
    $(".next_level").click(function () {
        $(".paskaa").show();
        $(".asiaa").hide();
        $(document.body).addClass("ripulipaskaa");
        $("audio").prop("autoplay", "autoplay");
    });

    // Vielä vähän
    $("kallunki_matikka.html").on("load");
    {
        if (localStorage.getItem("perseily") === "on") {
            $(".paskaa").show();
            $(".asiaa").hide();
            $(document.body).addClass("ripulipaskaa");
            $("audio").prop("autoplay", "autoplay");
            localStorage.setItem("perseily", "off");
        }
    }

    // Uusi tehtävä
    function newTask() {
        tehtava_nro += 1;
        pallot = 0;
        nro = 0;

        $("#tehtava_nro").html(tehtava_nro);
        $("[name=lv]").prop("disabled", false);
        $("#ohita2").prop("disabled", false);
        $("#tarkista").prop("disabled", false);
        $("#tarkista").removeClass("oikein");
        $("#tarkista").removeClass("vaarin");
        $(".ov2").html("");
        $("#selitys2").html("");
        $("#seuraava2").hide();

        // Riippuen vaikeusasteesta, valitsee asetettavan luvun ja tarkistaa ettei tehtävää ole jo tehty
        if (vaikeus === 1) {
            nro = getRndInteger(0, 10);
            for (let i = 0; ; i++) {
                if (tehdyt.includes(nro) === true) {
                    nro = getRndInteger(0, 10);
                } else {
                    break;
                }
            }
            for (let i = 0; ; i++) {
                pallot = getRndInteger(0, 10);
                if (pallot !== nro) {
                    break;
                }
            }
        } else if (vaikeus === 2) {
            nro = getRndInteger(5, 20);
            for (let i = 0; ; i++) {
                if (tehdyt.includes(nro) === true) {
                    nro = getRndInteger(5, 20);
                } else {
                    break;
                }
            }
            for (let i = 0; ; i++) {
                pallot = getRndInteger(5, 20);
                if (pallot !== nro) {
                    break;
                }
            }
        } else if (vaikeus === 3) {
            nro = getRndInteger(10, 30);
            for (let i = 0; ; i++) {
                if (tehdyt.includes(nro) === true) {
                    nro = getRndInteger(10, 30);
                } else {
                    break;
                }
            }
            for (let i = 0; ; i++) {
                pallot = getRndInteger(10, 30);
                if (pallot !== nro) {
                    break;
                }
            }
        }
        $("#tehtava").html(nro);
        tehdyt.push(nro);
        hanki_pallit();
    }

    // Pallojen määrän alkuasetus
    function hanki_pallit() {
        $("#pallot").html("");
        for (let i = 0; i < pallot; i++) {
            $("#pallot").append('<span class="fa fa-circle fa-4x" aria-hidden="true"></span>');
        }
    }

    // Kun painetaan vaikeusastetta
    $(".vaikeus").click(function () {
        if ($(this).hasClass("next_level") === false) {
            $("#lukujonot").hide();
            $(".matikka").show();
            vaikeus = Number($(this).val());
            newTask();
        }
    });

    // Lisää tai vähennä pallukoita
    $("[name=lv]").click(function () {
        $("#pallot span").removeClass("remove");
        $("#pallot span").removeClass("add");
        if ($(this).val() === "+1" && pallot < 35) {
            pallot += 1;
            $("#pallot").append('<span class="fa fa-circle fa-4x add" aria-hidden="true"></span>');
        } else if ($(this).val() === "-1" && pallot > 0) {
            pallot -= 1;
            $("#pallot span:last-child").remove();
            $("#pallot span:last-child").addClass("remove");
        }
    });

    // Tarkista onko ratkaistu oikein
    $("#tarkista").click(function () {
        $("[name=lv]").prop("disabled", true);
        $("#ohita2").prop("disabled", true);
        $("#seuraava2").show();
        $(this).prop("disabled", true);
        vastatut2 += 1;

        if (pallot === nro) {
            $("#tarkista").addClass("oikein");
            $(this).next().html('<span>Oikein! </span><span class="fa fa-check fa-lg" aria-hidden="true"></span>');
            $("#selitys2").html("Hyvä! Palloja on juuri oikea määrä.");
            pisteet2 += 1;
        } else {
            $("#tarkista").addClass("vaarin");
            $(this).next().html('<span>Väärin! </span><span class="fa fa-times fa-lg" aria-hidden="true"></span>');
            let ero = nro - pallot;

            if (ero > 0) {
                $("#selitys2").html("Ratkaisu: Palloja tulisi olla " + ero + " enemmän.");
            } else {
                $("#selitys2").html("Ratkaisu: Palloja tulisi olla " + Math.abs(ero) + " vähemmän.");
            }
        }
    });

    // Päättyy ja palkinnot
    function endMath() {
        $(".matikka").hide();
        $(".loppu2").show();
        $("#vastatut2").html(vastatut2);
        $("#pisteet2").html(pisteet2);

        if (pisteet2 === 10 || pisteet2 === 9) {
            // Kultaa
            $("#ilme2").html('<span class="fa fa-smile-o fa-5x mt-3" aria-hidden="true"></span><span class="fa fa-thumbs-o-up fa-5x mt-3 ml-3 swing" aria-hidden="true"></span>');
            $("#lause2").html("Ratkaisit todella monta tehtävää. Mainiota!");
            $("#palkinto2").html('<img src="img/gold.jpg" alt="Kultaa" class="img-fluid"><p class="pl-1">Kultaa palkinnoksi</p>');
        } else if (pisteet2 < 9 && pisteet2 > 3) {
            // Hopeaa
            $("#ilme2").html('<span class="fa fa-smile-o fa-5x mt-3" aria-hidden="true"></span><span class="fa fa-thumbs-o-up fa-5x mt-3 ml-3 swing" aria-hidden="true"></span>');
            $("#lause2").html("Hyvin tehty! Sait monta tehtävää oikein.");
            $("#palkinto2").html('<img src="img/silver.jpg" alt="Hopeaa" class="img-fluid"><p class="pl-1">Hopeaa palkinnoksi</p>');
        } else if (pisteet2 < 4 && pisteet2 > 0) {
            // Pronssia
            $("#ilme2").html('<span class="fa fa-smile-o fa-5x mt-3" aria-hidden="true"></span><span class="fa fa-thumbs-o-up fa-5x mt-3 ml-3 swing" aria-hidden="true"></span>');
            $("#lause2").html("Ei huono! Osasit jotakin.");
            $("#palkinto2").html('<img src="img/bronze.jpg" alt="Pronssia" class="img-fluid"><p class="pl-1">Pronssia palkinnoksi</p>');
        } else if (pisteet2 === 0) {
            // NASSE-SETÄ
            $("#ilme2").html('<span class="fa fa-frown-o fa-5x mt-3" aria-hidden="true"></span><span class="fa fa-thumbs-o-down fa-5x mt-3 ml-3 swing" aria-hidden="true"></span>');
            $("#lause2").html("NASSE-SETÄ ON HYVIN VIHAINEN!!!");
            $("#palkinto2").html('<iframe controls autoplay allow="autoplay" src="https://www.youtube.com/embed/HpmKykMT1JU?&autoplay=1" alt="Nasse-setä"></iframe>');
        }

        tallennaPisteet("piste12", pisteet2);

    }

    // Seuraava kysymys - sekä Seuraava että Ohita -napille
    $("#seuraava2, #ohita2").click(function () {
        if (tehtava_nro < 10) {
            newTask();
        } else {
            endMath();
        }
    });

    // Pelaa uudelleen
    $("#uudelleen2").click(function () {
        $(".loppu2").hide();
        $("#lukujonot").show();
        tehtava_nro = 0;
        vastatut2 = 0;
        pisteet2 = 0;
        tehdyt = [];
        $("#palkinto2").html("");
    });

    // Etusivuscriptit
    // Lista linkeille
    let linkit = [
        ['Englanti <span class="fa fa-book fa-lg" aria-hidden="true"></span>', "iida.enkkuvisa.html"],
        ['Historia <span class="fa fa-book fa-lg" aria-hidden="true"></span>', "Jani.Historiavisa.html"],
        ['Uskonto <span class="fa fa-book fa-lg" aria-hidden="true"></span>', "Oskari.uskonto.html"],
        ['Biologia <span class="fa fa-globe fa-lg" aria-hidden="true"></span>', "marjaana_bilsa.html"],
        ['Maantieto <span class="fa fa-globe fa-lg" aria-hidden="true"></span>', "kallunki_visa.html"],
        ['Lajituntemus <span class="fa fa-globe fa-lg" aria-hidden="true"></span>', "Justus.Vainio.html"],
        ['Fysiikka <span class="fa fa-globe fa-lg" aria-hidden="true"></span>', "Saku.Physiikkavisa.html"],
        ['Yhteenlasku <span class="fa fa-calculator fa-lg" aria-hidden="true"></span>', "Oskari.matikka.html"],
        ['Vähennyslasku <span class="fa fa-calculator fa-lg" aria-hidden="true"></span>', "iida.matikkavisa.html"],
        ['Kertolasku <span class="fa fa-calculator fa-lg" aria-hidden="true"></span>', "marjaana_matikka.html"],
        ['Jakolasku <span class="fa fa-calculator fa-lg" aria-hidden="true"></span>', "Saku.Matikkavisa.html"],
        ['Lukujonot <span class="fa fa-calculator fa-lg" aria-hidden="true"></span>', "kallunki_matikka.html"],
        ['Geometria <span class="fa fa-calculator fa-lg" aria-hidden="true"></span>', "Justus.matikka.html"],
        ['Prosenttilaskut <span class="fa fa-calculator fa-lg" aria-hidden="true"></span>', "Jani.matikkavisa.html"],
        ['Persepano <span class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></span>', "kallunki_matikka.html"]
    ];

    // Arvo ehdotuksia sivun alussa
    let puuha = getRndInteger(0, linkit.length - 1);
    $("#puuhat1").html(linkit[puuha][0]);
    $("#puuhat1").attr("href", linkit[puuha][1]);
    linkit.splice(puuha, 1);
    puuha = getRndInteger(0, linkit.length - 1);
    $("#puuhat2").html(linkit[puuha][0]);
    $("#puuhat2").attr("href", linkit[puuha][1]);
    linkit.splice(puuha, 1);
    puuha = getRndInteger(0, linkit.length - 1);
    $("#puuhat3").html(linkit[puuha][0]);
    $("#puuhat3").attr("href", linkit[puuha][1]);
    linkit.splice(puuha, 1);

    // Ehdotusnapit
    $("[name=puuhanappi]").click(function () {
        // Taas tätä
        if ($(this).html() === 'Persepano <span class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></span>') {
            localStorage.setItem("perseily", "on");
        }

        window.location.href = $(this).attr("href");
    });

    // Tulostaulu: Tulostaa talletetut parhaat pisteet näkyviin ja laskee yhteispisteet, lisäksi maalaa pisteiden määrästä riippuen eri taustan
    for (let i = 1; i < 15; i++) {
        if (localStorage.getItem("piste" + i)) {
            $("[id=piste" + i + "]").html(localStorage.getItem("piste" + i));
            let vertaa = Number($("[id=piste" + i + "]").next().html());
            if ($("[id=piste" + i + "]").html() >= vertaa) {
                // Täydet pisteet
                $("[id=piste" + i + "]").addClass("perfecto");
                $("[id=piste" + i + "]").append(' <span class="fa fa-trophy fa-sm" aria-hidden="true"></span>');
            } else if ($("[id=piste" + i + "]").html() === "0") {
                // 0 pistettä
                $("[id=piste" + i + "]").addClass("pelattu_paskasti");
                $("[id=piste" + i + "]").append(' <span class="fa fa-sign-out fa-sm" aria-hidden="true"></span> <span class="fa fa-wheelchair-alt fa-sm" aria-hidden="true"></span> <span class="fa fa-bus fa-sm" aria-hidden="true"></span>');
            } else {
                // Siltä väliltä
                $("[id=piste" + i + "]").addClass("pelattu");
                $("[id=piste" + i + "]").append(' <span class="fa fa-star fa-sm" aria-hidden="true"></span>');
            }
        } else {
            $("[id=piste" + i + "]").html("");
        }
    }
    let yhteensa = 0;
    localStorage.setItem("piste15", yhteensa);
    for (let i = 1; i < 15; i++) {
        yhteensa += Number(localStorage.getItem("piste" + i));
    }
    $("[id=piste15]").html(yhteensa);
    
    // Tarkistaa, onko pelikerta ensimmäinen ja tallentaa pisteet
    // Jos ei ole eka kerta, vertaa aikaisempiin pisteisiin
    function tallennaPisteet(paikka, pisteet) {
        if (!localStorage.getItem(paikka)) {
            localStorage.setItem(paikka, pisteet);
        } else {
            if (localStorage.getItem(paikka) < pisteet) {
                localStorage.setItem(paikka, pisteet);
            }
        }
    }

});