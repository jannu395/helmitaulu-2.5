
$(document).ready(function() {
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    /*takasivun pisteet*/
    function tallennaPisteet(paikka, pisteet) {
        if (!localStorage.getItem(paikka)) {
            localStorage.setItem(paikka, pisteet);
        } else {
            if (localStorage.getItem(paikka) < pisteet) {
                localStorage.setItem(paikka, pisteet);
            }
        }
    }

    //enkkua
    let pisteet = 0;

    $(".vastaus").click(function() {
        let vastaus = Number($(this).val());
        let kysymys = $(this).attr("name");
        $("[name=" + kysymys + "]").prop("disable", true);

    //1 monivalintojen oikea vastaus
        if (vastaus === 1) {
            $(this).addClass("oikein");
            $(this).parent().addClass("oikein");
            pisteet++;
        } else {
            $(this).parent().addClass("vaarin");
            $("[name=" + kysymys + "][value=1]").next().addClass("oikein");
        }
    });

    //tehtävät näkyviin 1 kerrallaan, vastattu menee piiloon
    kysymykset = ["#kysymys1", "#kysymys2", "#kysymys3", "#kysymys4", "#kysymys5", "#kysymys6", "#kysymys7", "#kysymys8", "#kysymys9", "#kysymys10", "#kysymys11"];

    $(".jatkuu").click(function() {
        if (kysymykset.length > 0) {
        let nakyviin = kysymykset[0];
        $(nakyviin).show();
        kysymykset.splice(0,1);
            $(this).parent().hide();
        } else {
        }
    });

    //käännösten oikeat vastaukset
        $(".tarkista").click(function() {
        let ans = $("#vastaus1").val();
        let oikeavastaus = "children";
        if (ans === oikeavastaus) {
            $("#tulos1").html("That's correct!").addClass("oikein");
            pisteet++;
        } else {
            $("#tulos1").html("Wrong answer!").addClass("vaarin");
        }
    });
        $(".tarkista2").click(function() {
        let ans = $("#vastaus2").val();
        let oikeavastaus = "teeth";
        if (ans === oikeavastaus) {
            $("#tulos2").html("That's correct!").addClass("oikein");
            pisteet++;
        } else {
            $("#tulos2").html("Wrong answer!").addClass("vaarin");
        }
    });
        $(".tarkista3").click(function() {
        let ans = $("#vastaus3").val();
        let oikeavastaus = "mice";
        if (ans === oikeavastaus) {
            $("#tulos3").html("That's correct!").addClass("oikein");
            pisteet++;
        } else {
            $("#tulos3").html("Wrong answer!").addClass("vaarin");
        }
    });
        $(".tarkista3").click(function() {
        let ans = $("#vastaus3").val();
        let oikeavastaus = "mice";
        if (ans === oikeavastaus) {
            $("#tulos3").html("That's correct!").addClass("oikein");
            pisteet++;
        } else {
            $("#tulos3").html("Wrong answer!").addClass("vaarin");
        }
    });

        $(".tarkista4").click(function() {
        let ans = $("#vastaus4").val();
        let oikeavastaus = "churches";
        if (ans === oikeavastaus) {
            $("#tulos4").html("That's correct!").addClass("oikein");
            pisteet++;
        } else {
            $("#tulos4").html("Wrong answer!").addClass("vaarin");
        }
    });

        $(".tarkista5").click(function() {
        let ans = $("#vastaus5").val();
        let oikeavastaus = "leaves";
        if (ans === oikeavastaus) {
            $("#tulos5").html("That's correct!").addClass("oikein");
            pisteet++;
        } else {
            $("#tulos5").html("Wrong answer!").addClass("vaarin");
        }
    });
        $(".tarkista6").click(function() {
        let ans = $("#vastaus6").val();
        let oikeavastaus = "knives";
        if (ans === oikeavastaus) {
            $("#tulos6").html("That's correct!").addClass("oikein");
            pisteet++;
        } else {
            $("#tulos6").html("Wrong answer!").addClass("vaarin");
        }
    });
        $(".tarkista7").click(function() {
        let ans = $("#vastaus7").val();
        let oikeavastaus = "fishes";
        if (ans === oikeavastaus) {
            $("#tulos7").html("That's correct!").addClass("oikein");
            pisteet++;
        } else {
            $("#tulos7").html("Wrong answer!").addClass("vaarin");
        }
    });
        $(".tarkista8").click(function() {
        let ans = $("#vastaus8").val();
        let oikeavastaus = "feet";
        if (ans === oikeavastaus) {
            $("#tulos8").html("That's correct!").addClass("oikein");
            pisteet++;
        } else {
            $("#tulos8").html("Wrong answer!").addClass("vaarin");
        }
    });

    //kun visa loppuu, tulokset ja kuva vaihtuu
    $(".tulokset").click(function() {
        $("#end").removeClass("hidden");
        $(this).parent().hide();
        $("#ben").addClass("hidden");
        $("#tulos").html(pisteet);
        if (pisteet === 12 || 11) {
            $("#nauha").removeClass("hidden");
        }
            tallennaPisteet("piste1", pisteet);
    });

    //matikka, tarvittavia numeroita
    let pojot = 0;
    helpot_laskut = ["#lasku1", "#lasku2", "#lasku3", "#lasku4", "#lasku5", "#lasku6"];
    vaikeat_laskut = ["#lasku8", "#lasku9", "#lasku10", "#lasku11", "#lasku12", "#lasku13", "#lasku14"];
    let eka = getRndInteger(9,10);
    let toka = getRndInteger(1,9);
    let kolmas = getRndInteger(9,10);
    let neljas = getRndInteger(1,9);
    let viides = getRndInteger(9,10);
    let kuudes = getRndInteger(1,9);
    let seitsemas = getRndInteger(9,10);
    let kahdeksas = getRndInteger(1,9);
    let yhdeksas = getRndInteger(9,10);
    let kymmenes = getRndInteger(1,9);
    let yytoo = getRndInteger(9,10);
    let kaatoo = getRndInteger(1,9);
    let kootoo = getRndInteger(9,10);
    let neetoo = getRndInteger(1,9);
    $("#eka_nro").html(eka);
    $("#toka_nro").html(toka);
    $("#kolmas_nro").html(kolmas);
    $("#neljas_nro").html(neljas);
    $("#viides_nro").html(viides);
    $("#kuudes_nro").html(kuudes);
    $("#seiska_nro").html(seitsemas);
    $("#kasi_nro").html(kahdeksas);
    $("#ysi_nro").html(yhdeksas);
    $("#kymmenen_nro").html(kymmenes);
    $("#ekaeka_nro").html(yytoo);
    $("#ekatoka_nro").html(kaatoo);
    $("#ekakolmas_nro").html(kootoo);
    $("#ekaneljas_nro").html(neetoo);
    
    
    //kuva pois kun tehtävä alkaa
    $("#helppo").click(function() {
        $("#cal").addClass("hidden");
    });
    $("#vaikea").click(function() {
        $("#cal").addClass("hidden");
    });

//helpot tehtavat näkyviin jos niin valitaan, vastattu meneee piiloon
    $(".helppoihin_tehtaviin").click(function () {
        if (helpot_laskut.length > 0) {
            let nakyviin = helpot_laskut[0];
            $(nakyviin).show();
            helpot_laskut.splice(0,1);
            $(this).parent().hide();
        } else {
        }
    });
    
//vaikeat tehtävät näkyviin
    $(".vaikeisiin_tehtaviin").click(function () {
        if (vaikeat_laskut.length > 0) {
            let nakyviin = vaikeat_laskut[0];
            
            console.log(nakyviin);
            
            $(nakyviin).show();
            vaikeat_laskut.splice(0,1);
            $(this).parent().hide();
        } else {
        }
    });
    
// ratkaisut helppoihin tehtäviin  
    $("#oikea_erotus1").click(function () {
        let vastaus = parseInt($("#erotus1").val());
        let oikea_vastaus = parseInt($("#eka_nro").html()) - parseInt($("#toka_nro").html());
        if (vastaus === oikea_vastaus) {
            $("#ratkaisu1").html("Oikein!").addClass("oikein");
            pojot++;
        } else {
            $("#ratkaisu1").html("Väärin").addClass("vaarin");
        }
    });
    
        $("#oikea_erotus2").click(function () {
        let vastaus = parseInt($("#erotus2").val());
        let oikea_vastaus = parseInt($("#kolmas_nro").html()) - parseInt($("#neljas_nro").html());
        if (vastaus === oikea_vastaus) {
            $("#ratkaisu2").html("Oikein!").addClass("oikein");
            pojot++;
        } else {
            $("#ratkaisu2").html("Väärin!").addClass("vaarin");
        }
    });  
    
    $("#oikea_erotus3").click(function () {
        let vastaus = parseInt($("#erotus3").val());
        let oikea_vastaus = parseInt($("#viides_nro").html()) - parseInt($("#kuudes_nro").html());
        if (vastaus === oikea_vastaus) {
            $("#ratkaisu3").html("Oikein!").addClass("oikein");
            pojot++;
        } else {
            $("#ratkaisu3").html("Väärin!").addClass("vaarin");
        }
    });
    
    $("#oikea_erotus4").click(function () {
        let vastaus = parseInt($("#erotus4").val());
        let oikea_vastaus = parseInt($("#seiska_nro").html()) - parseInt($("#kasi_nro").html());
        if (vastaus === oikea_vastaus) {
            $("#ratkaisu4").html("Oikein!").addClass("oikein");
            pojot++;
        } else {
            $("#ratkaisu4").html("Väärin!").addClass("vaarin");
        }
    });
    
    $("#oikea_erotus5").click(function () {
        let vastaus = parseInt($("#erotus5").val());
        let oikea_vastaus = parseInt($("#ysi_nro").html()) - parseInt($("#kymmenen_nro").html());
        if (vastaus === oikea_vastaus) {
            $("#ratkaisu5").html("Oikein!").addClass("oikein");
            pojot++;
        } else {
            $("#ratkaisu5").html("Väärin!").addClass("vaarin");
        }
    });
    
    $("#oikea_erotus6").click(function () {
        let vastaus = parseInt($("#erotus6").val());
        let oikea_vastaus = parseInt($("#ekaeka_nro").html()) - parseInt($("#ekatoka_nro").html());
        if (vastaus === oikea_vastaus) {
            $("#ratkaisu6").html("Oikein!").addClass("oikein");
            pojot++;
        } else {
            $("#ratkaisu6").html("Väärin!").addClass("vaarin");
        }
    });
    
    $("#oikea_erotus7").click(function () {
        let vastaus = parseInt($("#erotus7").val());
        let oikea_vastaus = parseInt($("#ekakolmas_nro").html()) - parseInt($("#ekaneljas_nro").html());
        if (vastaus === oikea_vastaus) {
            $("#ratkaisu7").html("Oikein!").addClass("oikein");
            pojot++;
        } else {
            $("#ratkaisu7").html("Väärin!").addClass("vaarin");
        }
    });
 //lisää numeroita, nämä vaikeisiin tehtäviin
    let toinen_eka = getRndInteger(15,20);
    let toinen_toka = getRndInteger(1,14);
    let toinen_kolmas = getRndInteger (15,20);
    let toinen_neljas = getRndInteger(1,14);
    let toinen_viides = getRndInteger (15,20);
    let toinen_kuudes = getRndInteger(1,15);
    let toinen_seitsemas = getRndInteger (20,27);
    let toinen_kahdeksas = getRndInteger(1,20);
    let toinen_yhdeksas = getRndInteger (20,27);
    let toinen_kymmenes = getRndInteger(1,25);
    let toinen_yytoo = getRndInteger (29,30);
    let toinen_kaatoo = getRndInteger (1,28);
    let toinen_kootoo = getRndInteger (30,31);
    let toinen_neetoo = getRndInteger (1,29);
    $("#ekaviides_nro").html(toinen_eka);
    $("#ekakuudes_nro").html(toinen_toka);
    $("#ekaseiska_nro").html(toinen_kolmas);
    $("#ekakasi_nro").html(toinen_neljas);
    $("#ekaysi_nro").html(toinen_viides);
    $("#kakskyt_nro").html(toinen_kuudes);
    $("#tokaeka_nro").html(toinen_seitsemas);
    $("#tokatoka_nro").html(toinen_kahdeksas);
    $("#tokakolmas_nro").html(toinen_yhdeksas);
    $("#tokaneljas_nro").html(toinen_kymmenes);
    $("#tokaviides_nro").html(toinen_yytoo);
    $("#tokakuudes_nro").html(toinen_kaatoo);
    $("#tokaseiska_nro").html(toinen_kootoo);
    $("#tokakasi_nro").html(toinen_neetoo);
    
    //vaikeiden tehtävien ratkaisut
    $("#oikea_erotus8").click(function () {
        let vastaus = parseInt($("#erotus8").val());
        let oikea_vastaus = parseInt($("#ekaviides_nro").html()) - parseInt($("#ekakuudes_nro").html());
        if (vastaus === oikea_vastaus) {
            $("#ratkaisu8").html("Oikein!").addClass("oikein");
            pojot++;
        } else {
            $("#ratkaisu8").html("Väärin!").addClass("vaarin");
        }
    });
    $("#oikea_erotus9").click(function () {
        let vastaus = parseInt($("#erotus9").val());
        let oikea_vastaus = parseInt($("#ekaseiska_nro").html()) - parseInt($("#ekakasi_nro").html());
        if (vastaus === oikea_vastaus) {
            $("#ratkaisu9").html("Oikein!").addClass("oikein");
            pojot++;
        } else {
            $("#ratkaisu9").html("Väärin!").addClass("vaarin");
        }
    });
    $("#oikea_erotus10").click(function () {
        let vastaus = parseInt($("#erotus10").val());
        let oikea_vastaus = parseInt($("#ekaysi_nro").html()) - parseInt($("#kakskyt_nro").html());
        if (vastaus === oikea_vastaus) {
            $("#ratkaisu10").html("Oikein!").addClass("oikein");
            pojot++;
        } else {
            $("#ratkaisu10").html("Väärin!").addClass("vaarin");
        }
    });
    $("#oikea_erotus11").click(function () {
        let vastaus = parseInt($("#erotus11").val());
        let oikea_vastaus = parseInt($("#tokaeka_nro").html()) - parseInt($("#tokatoka_nro").html());
        if (vastaus === oikea_vastaus) {
            $("#ratkaisu11").html("Oikein!").addClass("oikein");
            pojot++;
        } else {
            $("#ratkaisu11").html("Väärin!").addClass("vaarin");
        }
    });
    $("#oikea_erotus12").click(function () {
        let vastaus = parseInt($("#erotus12").val());
        let oikea_vastaus = parseInt($("#tokakolmas_nro").html()) - parseInt($("#tokaneljas_nro").html());
        if (vastaus === oikea_vastaus) {
            $("#ratkaisu12").html("Oikein!").addClass("oikein");
            pojot++;
        } else {
            $("#ratkaisu12").html("Väärin!").addClass("vaarin");
        }
    });
    $("#oikea_erotus13").click(function () {
        let vastaus = parseInt($("#erotus13").val());
        let oikea_vastaus = parseInt($("#tokaviides_nro").html()) - parseInt($("#tokakuudes_nro").html());
        if (vastaus === oikea_vastaus) {
            $("#ratkaisu13").html("Oikein!").addClass("oikein");
            pojot++;
        } else {
            $("#ratkaisu13").html("Väärin!").addClass("vaarin");
        }
    });
        $("#oikea_erotus14").click(function () {
        let vastaus = parseInt($("#erotus14").val());
        let oikea_vastaus = parseInt($("#tokaseiska_nro").html()) - parseInt($("#tokakasi_nro").html());
        if (vastaus === oikea_vastaus) {
            $("#ratkaisu14").html("Vautsi vau!").addClass("oikein");
            pojot++;
        } else {
            $("#ratkaisu14").html("Väärin!").addClass("vaarin");
        }
    });
    
    $(".tulokset_matikka").click(function () {
        $("#mathend").removeClass("hidden");
        $(this).parent().hide();
        $("#cal").addClass("hidden");
        $("#pojot").html(pojot);
        if (pojot === 6) {
            $("#cal2").removeClass("hidden");
            $("#nauha").removeClass("hidden");
        }
        if (pojot === 7) {
            $("#pingu").removeClass("hidden");
            $("#akumu").removeClass("hidden");
        }
        else {
            $("#cal2").removeClass("hidden");
        }
            tallennaPisteet("piste9", pojot);
    });
    
    
    //kysyttavat
    $(".hint").popover({
        trigger: "focus"
    });
});


